﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LT_Complex
{
    class Complex
    {
        public double Real { get; set; } // Действительная часть
        public double Imaginary { get; set; } // Мнимая часть


        public Complex (double real, double imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }

        public override string ToString()
        {
            return $"{Real} + {Imaginary}i";
        }

        public static Complex operator + (Complex a, Complex b)
        {
            return new Complex(a.Real + b.Real, a.Imaginary + b.Imaginary);
        }

        public static Complex operator + (Complex a, double b)
        {
            return new Complex(b + a.Real, a.Imaginary);
        }

        public static Complex operator + (double a, Complex b)
        {
            return new Complex(a + b.Real, b.Imaginary);
        }

        public static Complex operator - (Complex a, Complex b)
        {
            return new Complex(a.Real - b.Real, a.Imaginary - b.Imaginary);
        }

        public static Complex operator * (Complex a, Complex b)
        {
            return new Complex(a.Real * b.Real-a.Imaginary*b.Imaginary, a.Imaginary*b.Real+a.Real*b.Imaginary);
        }

        public static Complex operator / (Complex a, Complex b)
        {
            return new Complex((a.Real*b.Real+a.Imaginary*b.Imaginary)/(b.Real*b.Real+b.Imaginary*b.Imaginary), (a.Imaginary*b.Real-a.Real*b.Imaginary)/(b.Real*b.Real+b.Imaginary*b.Imaginary));
        }
    }
}
